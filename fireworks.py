import pygame
import sys
import random
import math
from defs import *

from particle import Particle
from sparker import Sparker
import util

class Fireworks(object):
    def __init__(self, num_fireworks, thresh_time):
        super(Fireworks, self).__init__()
        self.num_fireworks = num_fireworks
        self.done = False
        self.cumulatime = 0
        self.thresh_time = thresh_time
        self.FPSClock = pygame.time.Clock()
        self.blackSurf = pygame.Surface(resolution).convert_alpha()
        self.blackSurf.fill(BLACK_FADED)

    def show_works(self, screen):
        if self.num_fireworks > 0:
            self.num_fireworks -= 1
            self.make_fireworks()
        screen.blit(self.blackSurf, (0,0))
        dt = self.FPSClock.tick(FPS) / 60.0
        p_count =  0
        for p in Particle.allParticles:
            p_count += 1
            p.update(dt)
            p.draw(screen)

        s_count =  0
        for s in Sparker.allSparkers:
            s_count += 1
            s.update(dt)
            s.draw(screen)
        if self.cumulatime > self.thresh_time:
            if p_count ==  0:
                if s_count ==  0:
                    if self.num_fireworks < 1:
                            self.done = True

        screen.blit(screen, (0, 0))
        #pygame.display.update()

        #pygame.display.set_caption("Fireworks  (FPS " + str(round(FPSClock.get_fps(), 1)) + ")")
    # click to spawn fireworks, escape to quit
    def make_fireworks(self):
        Sparker(pos=list((random.randint(0,1200),random.randint(0,1200))),
                colour=[random.uniform(0, 255),
                        random.uniform(0, 255),
                        random.uniform(0, 255)],
                velocity=random.uniform(40, 60),
                particleSize=random.uniform(10, 20),
                sparsity=random.uniform(0.05, 0.15),
                hasTrail=True,
                lifetime=random.uniform(10, 20),
                isShimmer=False)
        Sparker(pos=list((random.randint(0,1200),random.randint(0,1200))),
                colour=[random.uniform(50, 255),
                        random.uniform(50, 255),
                        random.uniform(50, 255)],
                velocity=random.uniform(1, 2),
                particleSize=random.uniform(3, 8),
                sparsity=random.uniform(0.05, 0.15),
                hasTrail=False,
                lifetime=random.uniform(20, 30),
                isShimmer=True,
                radius=random.uniform(40, 100),
                proportion=0.6,
                focusRad=random.choice([0, 0.4, 3, 6]),
                weight=random.uniform(0.001, 0.0015))

    def is_done(self):
        return self.done

    def add_time(self, cumulatime):
        #print(self.cumulatime)
        self.cumulatime += cumulatime
