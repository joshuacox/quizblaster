'''The MIT License (MIT)

Copyright (c) 2017 ActiveState Software Inc.

Written by Pete Garcin @rawktron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.'''

# game.py
# Core game initialization, main game loop, etc.

from __future__ import division
import pygame
#import pygame.freetype
import math
import socket as this_socket
import numpy as np
import gamestates
import sys
import os
import random
import argparse
from data import error_print
from defs import *
from utils import *
from particle import Particle
from sparker import Sparker

# click to spawn fireworks, escape to quit
def make_fireworks():
    Sparker(pos=list((random.randint(0,1200),random.randint(0,1200))),
            colour=[random.uniform(0, 255),
                    random.uniform(0, 255),
                    random.uniform(0, 255)],
            velocity=random.uniform(40, 60),
            particleSize=random.uniform(10, 20),
            sparsity=random.uniform(0.05, 0.15),
            hasTrail=True,
            lifetime=random.uniform(10, 20),
            isShimmer=False)
    Sparker(pos=list((random.randint(0,1200),random.randint(0,1200))),
            colour=[random.uniform(50, 255),
                    random.uniform(50, 255),
                    random.uniform(50, 255)],
            velocity=random.uniform(1, 2),
            particleSize=random.uniform(3, 8),
            sparsity=random.uniform(0.05, 0.15),
            hasTrail=False,
            lifetime=random.uniform(20, 30),
            isShimmer=True,
            radius=random.uniform(40, 100),
            proportion=0.6,
            focusRad=random.choice([0, 0.4, 3, 6]),
            weight=random.uniform(0.001, 0.0015))

def handleInput():
    for event in pygame.event.get():
        # terminate on system quit or esc key
        if event.type == pygame.QUIT or pygame.key.get_pressed()[pygame.K_ESCAPE]:
            pygame.quit()
            sys.exit()

        # spawn fireworks on click
        if event.type == pygame.MOUSEBUTTONDOWN:

            if event.button == FIREWORK_MOUSE_BUTTON:
                Sparker(pos=list(pygame.mouse.get_pos()),
                        colour=[random.uniform(0, 255),
                                random.uniform(0, 255),
                                random.uniform(0, 255)],
                        velocity=random.uniform(40, 60),
                        particleSize=random.uniform(10, 20),
                        sparsity=random.uniform(0.05, 0.15),
                        hasTrail=True,
                        lifetime=random.uniform(10, 20),
                        isShimmer=False)

            if event.button == SHIMMER_MOUSE_BUTTON:
                Sparker(pos=list(pygame.mouse.get_pos()),
                        colour=[random.uniform(50, 255),
                                random.uniform(50, 255),
                                random.uniform(50, 255)],
                        velocity=random.uniform(1, 2),
                        particleSize=random.uniform(3, 8),
                        sparsity=random.uniform(0.05, 0.15),
                        hasTrail=False,
                        lifetime=random.uniform(20, 30),
                        isShimmer=True,
                        radius=random.uniform(40, 100),
                        proportion=0.6,
                        focusRad=random.choice([0, 0.4, 3, 6]),
                        weight=random.uniform(0.001, 0.0015))
#write pid
def write_pid_to_pidfile(pidfile_path):
    if os.path.isfile(pidfile_path):
        os.remove(pidfile_path)
    open_flags = (os.O_CREAT | os.O_EXCL | os.O_WRONLY)
    open_mode = 0o644
    pidfile_fd = os.open(pidfile_path, open_flags, open_mode)
    pidfile = os.fdopen(pidfile_fd, 'w')
    pid = os.getpid()
    pidfile.write("%s\n" % pid)
    pidfile.close()

write_pid_to_pidfile('/tmp/quiz_pid')

### GLOBAL GAME INIT AND MAIN LOOP
# Basic library initialization
pygame.init()
pygame.mouse.set_visible(False)
loadfont(24)
parser = argparse.ArgumentParser()
parser.add_argument('-f',action='store_true')
parser.add_argument('-n',action='store_true')
parser.add_argument('-v',action='store_true')
parser.add_argument('-s',action='store_true')

args = parser.parse_args()

# 0 is internal neural net, 1 is keras/tensorflow (default)
netmodel = 1

# VizModel is which viz method, direct Keras (1) or simulated neural net (0) (faster)
vizmodel = 0

flags = pygame.DOUBLEBUF

show_fireworks = 0
# (-s) Show fps or not
if (args.s == True):
    show_fps = 1
else:
    show_fps = 0

if (args.f == True):
    flags |= pygame.FULLSCREEN

# Netmodel = 1 means Keras/Tensorflow, 0 = internal simple neural net for prototyping
if (args.n == True):
    netmodel = 0

if (args.v == True):
    vizmodel = 1

screen = pygame.display.set_mode(resolution, flags)
screen.set_alpha(None)

pygame.display.set_caption("QuizBlast")

#background = pygame.image.load('art/python-game_background.png')
background = pygame.image.load('images/rb-bg.png')
bgsize = background.get_size()
w, h = bgsize

# TODO Get dynamic resolution but width is X_BOUND for now

aspect = w/h
wscale = X_BOUND
hscale = wscale/aspect

#bgscaled = pygame.transform.scale(background, (X_BOUND, int(hscale)))

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Init gamepads
# Initialize the joystick control, get the first one
pygame.joystick.init()
if (pygame.joystick.get_count()>0):
    joystick = pygame.joystick.Joystick(0)
    joystick.init()
else:
    joystick = None

init_stars(screen)

# Initial game state is chosen by hostname
if this_socket.gethostname() == 'flux.thalhalla.net':
    state = gamestates.QuizMenu(None)
    #state = gamestates.QuizMenuSpec(None, 3)
elif this_socket.gethostname() == 'quizbox001':
    state = gamestates.QuizMenuSpec(None, 1)
elif this_socket.gethostname() == 'quizbox002':
    state = gamestates.QuizMenuSpec(None, 2)
elif this_socket.gethostname() == 'quizbox003':
    state = gamestates.QuizMenuSpec(None, 3)
else:
    state = gamestates.QuizMenu(None)

scrollSpeed = -1

# Optimized method for scrolling background continuously
topY = hscale-720       # As soon as topY because -720, next frame, flip it back to hscale-720

plotUpdateRate = 1/10.0
plotCounter = 0.0

while not done:
    event_queue = pygame.event.get()
    for event in event_queue:
        if event.type == pygame.QUIT:
            done = True

    topY += scrollSpeed
    offset = hscale+topY    # If topY becomes negative, we use this to seamlessly blit until it clears itself up
    y = topY
    blitStartY = 0
    height = 720
    if (topY<0):
        blitStartY = -topY
        height = 720+topY
        y = 0
    #    screen.blit(bgscaled,(0,0),(0,offset,X_BOUND,720-height))
    #screen.blit(bgscaled,(0,blitStartY),(0,y,X_BOUND,height+1))
    screen.fill(BG_COLOR)

    if topY<=-720:
        topY = hscale-720
    #move_and_draw_stars(screen)
    if show_fireworks == 1:
        blackSurf = pygame.Surface(resolution).convert_alpha()
        blackSurf.fill(BLACK_FADED)
        screen.blit(blackSurf, (0,0))
        FPSClock = pygame.time.Clock()
        dt = FPSClock.tick(FPS) / 60.0

        #handleInput()
        make_fireworks()

        for p in Particle.allParticles:
            p.update(dt)
            p.draw(screen)

        for s in Sparker.allSparkers:
            s.update(dt)
            s.draw(screen)

        screen.blit(screen, (0, 0))
        pygame.display.update()

        pygame.display.set_caption("Fireworks  (FPS " + str(round(FPSClock.get_fps(), 1)) + ")")

    if show_fps == 1:
        displaytext("FPS:{:.2f}".format(clock.get_fps()) , 16, FPS_POSITION, 20, YELLOW, screen)
    ## Gamestate update
    state = state.update(screen, event_queue, clock.get_time()/1000.0, clock, joystick, netmodel,vizmodel)
    ## Trap exits from gamestate
    if state == None:
        done = True

    pygame.display.flip()
    clock.tick(30)

# Close the window and quit.
pygame.quit()
